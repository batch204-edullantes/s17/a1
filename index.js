console.log('Hello World');

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

	function getPersonInformation() {
		let fullName = prompt('What is your name?');
		let age = prompt('How old are you? ');
		let location = prompt('Where do you live? ');

		console.log('Hello, ' + fullName);
		console.log('You are ' + age + ' years old');
		console.log('You live in ' + location);

		alert('Thank you for your input!');
	}

	getPersonInformation();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function printFavoriteBands() {
		const favoriteBands = ['The Beatles', 'Metallica', 'The Eagles', 'Eraserheads', 'Parokya ni Edgar'];

		console.log('1. ' + favoriteBands[0]);
		console.log('2. ' + favoriteBands[1]);
		console.log('3. ' + favoriteBands[2]);
		console.log('4. ' + favoriteBands[3]);
		console.log('5. ' + favoriteBands[4]);
	}

	printFavoriteBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function printFavoriteMoviesAndRottenTomatoeRating() {
		const moviesAndRatings = [
			{
				"movie" : 'The Godfather',
				'tomatoeRating' : '97%'
			},
			{
				"movie" : 'The Godfather, Part II',
				'tomatoeRating' : '96%'
			},
			{
				"movie" : 'Shawshank Redemption',
				'tomatoeRating' : '91%'
			},
			{
				"movie" : 'To Kill A Mockingbird',
				'tomatoeRating' : '93%'
			},
			{
				"movie" : 'Psycho',
				'tomatoeRating' : '96%'
			}
		]

		console.log('1. ' + moviesAndRatings[0].movie);
		console.log('Rotten Tomatoes Rating: ' + moviesAndRatings[0].tomatoeRating);
		console.log('2. ' + moviesAndRatings[1].movie);
		console.log('Rotten Tomatoes Rating: ' + moviesAndRatings[1].tomatoeRating);
		console.log('3. ' + moviesAndRatings[2].movie);
		console.log('Rotten Tomatoes Rating: ' + moviesAndRatings[2].tomatoeRating);
		console.log('4. ' + moviesAndRatings[3].movie);
		console.log('Rotten Tomatoes Rating: ' + moviesAndRatings[3].tomatoeRating);
		console.log('5. ' + moviesAndRatings[4].movie);
		console.log('Rotten Tomatoes Rating: ' + moviesAndRatings[4].tomatoeRating);
	}

	printFavoriteMoviesAndRottenTomatoeRating();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

	const printFriends = function printUsers(){
		alert("Hi! Please add the names of your friends.");
		let friend1 = prompt("Enter your first friend's name:"); 
		let friend2 = prompt("Enter your second friend's name:"); 
		let friend3 = prompt("Enter your third friend's name:");

		console.log("You are friends with:")
		console.log(friend1); 
		console.log(friend2); 
		console.log(friend3); 
	};

	printFriends();


// console.log(friend1);
// console.log(friend2);